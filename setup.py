import setuptools

with open('requirements.txt', 'r') as f:
    install_requires = f.read().splitlines()

setuptools.setup(
    name='raycasting',
    packages=['raycasting'],
    install_requires=install_requires
)
