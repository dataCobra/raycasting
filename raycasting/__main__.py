"""
raycasting | https://codeberg.org/dataCobra/raycasting

Copyright (C) 2022, Benedikt Brinkmann <datacobra@thinkbot.de>

raycasting is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

raycasting is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from game import Game

if __name__ == '__main__':
    game = Game()
    game.run()
