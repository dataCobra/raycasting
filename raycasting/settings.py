"""
raycasting | https://codeberg.org/dataCobra/raycasting

Copyright (C) 2022, Benedikt Brinkmann <datacobra@thinkbot.de>

raycasting is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

raycasting is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import math

# Graphics
WIDTH: int = 1600
HEIGHT: int = 900
RESOLUTION: int = (WIDTH, HEIGHT)
HALF_WIDTH: int= WIDTH // 2
HALF_HEIGHT: int= HEIGHT // 2
BOXES_X: int = 20
BOXES_Y: int = 15
CENTER: list[int] = (BOXES_X // 2, BOXES_Y // 2)
FPS: int = 0
GRID_X: float = WIDTH / BOXES_X
GRID_Y: float = HEIGHT / BOXES_Y

# Player
PLAYER_POS: list[int] = CENTER
PLAYER_ANGLE: int = 0
PLAYER_SPEED: float = 0.003
PLAYER_ROT_SPEED: float = 0.002
PLAYER_SIZE_SCALE: int = 60

# FOV
FOV = math.pi / 3
HALF_FOV = FOV / 2
NUM_RAYS = WIDTH // 2
HALF_NUM_RAYS = NUM_RAYS // 2
DELTA_ANGLE = FOV / NUM_RAYS
MAX_DEPTH = 20
SCREEN_DIST = HALF_WIDTH / math.tan(HALF_FOV)
SCALE = WIDTH // NUM_RAYS
