"""
raycasting | https://codeberg.org/dataCobra/raycasting

Copyright (C) 2022, Benedikt Brinkmann <datacobra@thinkbot.de>

raycasting is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

raycasting is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import sys

import pygame as pg

from map import Map
from player import Player
from raycasting import Raycasting
from settings import RESOLUTION, FPS, WIDTH
from colors import GRAY


class Game:
    def __init__(self):
        pg.init()
        self.screen = pg.display.set_mode(RESOLUTION)
        self.clock = pg.time.Clock()
        self.delta_time = 1
        self.font = pg.font.SysFont('DejaVu Sans Mono', 18)
        self.new_game()

    def new_game(self):
        self.map = Map(self)
        self.player = Player(self)
        self.raycasting = Raycasting(self)

    def update(self):
        self.player.update()
        self.raycasting.update()
        self.delta_time = self.clock.tick(FPS) + 0.0001  # Fix zero division
        self.fps = self.font.render(f'{self.clock.get_fps():.1f}', True, GRAY)
        self.screen.blit(self.fps, (WIDTH - self.fps.get_width(), 0))
        pg.display.flip()

    def draw(self):
        self.screen.fill('black')

    def check_events(self):
        for event in pg.event.get():
            if event.type == pg.QUIT:
                pg.quit()
                sys.exit()

    def run(self):
        while True:
            self.check_events()
            self.update()
            self.draw()
