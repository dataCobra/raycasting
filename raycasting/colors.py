"""
raycasting | https://codeberg.org/dataCobra/raycasting

Copyright (C) 2022, Benedikt Brinkmann <datacobra@thinkbot.de>

raycasting is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

raycasting is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

WHITE: tuple[int] = (255,255,255)
BLACK: tuple[int] = (0, 0, 0)
GRAY: tuple[int] = (175,175,175)
ORANGE: tuple[int] = (255,125,26)
GREEN: tuple[int] = (77,179,77)
RED: tuple[int] = (255,0,0)
