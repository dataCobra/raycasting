import math

import pygame as pg

from settings import *


class Raycasting:
    def __init__(self, game):
        self.game = game

    def ray_cast(self):
        px, py = self.game.player.pos
        map_x, map_y = self.game.player.map_pos

        ray_angle = self.game.player.angle - HALF_FOV + 0.0001
        for ray in range(NUM_RAYS):
            sin_a = math.sin(ray_angle)
            cos_a = math.cos(ray_angle)

            # horizontal
            hor_y, dy = (map_y + 1, 1) if sin_a > 0 else (map_y - 1e-6, -1)

            hor_depth = (hor_y - py) / sin_a
            hor_x = px + hor_depth * cos_a

            delta_depth = dy / sin_a
            dx = delta_depth * cos_a

            for i in range(MAX_DEPTH):
                hor_tile = int(hor_x), int(hor_y)
                if hor_tile in self.game.map.world_map:
                    break
                hor_x += dx
                hor_y += dy
                hor_depth += delta_depth

            # verticals
            vert_x, dx = (map_x + 1, 1) if cos_a > 0 else (map_x - 1e-6, -1)

            vert_depth = (vert_x - px) / cos_a
            vert_y = py + vert_depth * sin_a

            delta_depth = dx / cos_a
            dy = delta_depth * sin_a

            for i in range(MAX_DEPTH):
                vert_tile = int(vert_x), int(vert_y)
                if vert_tile in self.game.map.world_map:
                    break
                vert_x += dx
                vert_y += dy
                vert_depth += delta_depth

            # depth
            if vert_depth < hor_depth:
                depth = vert_depth
            else:
                depth = hor_depth

            # remove fishbowl effect
            depth *= math.cos(self.game.player.angle - ray_angle)

            # projection
            proj_height = SCREEN_DIST / (depth + 0.0001)

            # draw walls
            color = [255 / (1 + depth ** 5 * 0.00002)] * 3
            pg.draw.rect(self.game.screen, color,
                    (ray * SCALE, HALF_HEIGHT - proj_height // 2, SCALE, proj_height))

            ray_angle += DELTA_ANGLE

    def update(self):
        self.ray_cast()
