"""
raycasting | https://codeberg.org/dataCobra/raycasting

Copyright (C) 2022, Benedikt Brinkmann <datacobra@thinkbot.de>

raycasting is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

raycasting is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import random

import pygame as pg

from colors import GRAY
from settings import BOXES_X, BOXES_Y, GRID_X, GRID_Y

class Map:
    def __init__(self, game):
        self.game = game
        self.map_array: list[int, False] = [[]]
        self.world_map = {}
        self.new_map()
        self.get_map()

    def new_map(self):
        random.seed()
        self.map_array = [[1 for i in range(BOXES_X)] for j in range(BOXES_Y)]
        for j in range(1, BOXES_Y-1):
            for i in range(1, BOXES_X-1):
                self.map_array[j][i] = random.choices([1, False], [5, 12])[0]

    def get_map(self):
        for j, row in enumerate(self.map_array):
            for i, value in enumerate(row):
                if value:
                    self.world_map[(i, j)] = value

    def draw(self):
        for pos in self.world_map:
            pg.draw.rect(self.game.screen, GRAY, (pos[0] * GRID_X, pos[1] * GRID_Y, GRID_X, GRID_Y))
