# raycasting

A variant of the raycasting engine from Wolfenstein 3D implemented in Python
with the pygame module. As a reference I've used various different tutorials.

In the future I'll try to add new functionalities and modifications to the
raycasting engine.
